<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>épicerie</title>
    <link rel='stylesheet' href='style.css' type='text/css' />
  </head>
  <body>

    <form action="./ajout_produits.php" method="POST" enctype="multipart/form-data">
          <fieldset id="identity">
            <p>
              <label for="photo">Photo du produit </label><input type="file" name="photo" id="photo"/>
            </p>
            <p>
              <label for="Nom">Nom du produit </label><input type="text" name="Nom" id="nom"/>
            </p>
            <p>
              <label for="prix">Prix du produit </label><input type="text" name="prix" id="prix"/>
            </p>
            <p>
              <label for="qté">Quantité </label><input type="text" name="qté" id="qté"/>
            </p>
          </fieldset>
          <input type="submit" value="Envoyer"/>

    	</body>

      <?php
      define('TARGET_DIRECTORY', './photo/');
      if(!empty($_FILES['photo'])) {
        move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
      }
      if(isset($_POST['Nom'])){
        $name = $_POST['Nom'];
      }
      else{
        $name = null;
      }
      if(isset($_POST['prix'])){
        $price = $_POST['prix'];
      }
      else{
        $price = null;
      }

      if(isset($_POST['qté'])){
        $quantity = $_POST['qté'];
      }
      else{
        $quantity = null;
      }

        $resource = fopen("myproducts.csv", "a");
        fwrite($resource, $name.";");
        fwrite($resource, $price.";");
        fwrite($resource, $quantity."\n");

        $errors = handleFormErrors();

if(empty($errors)) {
    $path = handleProductImage();
    $name = getNameResult()['result'];
    $price = getPriceResult()['result'];
    $quantity = getQuantityResult()['result'];
    saveData(['name' => $name, 'price' => $price, 'quantity' => $quantity, 'img_path' => $path], 'save.csv');
}


</html>
